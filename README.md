# Shaft-a-bound

Easily deploy [Shaft's Unbound DMP blockers](https://framagit.org/Shaft/blocage-dmp-unbound) and update block lists with CLI.

Based on [Jee's Service Files](https://framagit.org/jee/scripts/tree/master/unbound).

**For Unbound 1.7.0+**

## Installation

```sh
git clone https://framagit.org/clov/shaftabound
cd shaftabound
sudo sh ./shaftabound install
```

## Usage

### Force list update

```sh
sudo sh ./shaftabound update
```

### Start Service

```sh
sudo sh ./shaftabound start
```

### Service status

```sh
sudo sh ./shaftabound status
```
